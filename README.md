# AdminBot

...is a Discord bot that aims to fully democratize the server that it is placed within.

It will accept initial input in the form of a message sent in any text channel.

Message must adhere to following protocol:

`!callvote command`

Where command is any operation that can be performed by a bot with Admin powers in a Discord server.

Includes:

- createTextChannel (name)
- createVoiceChannel (name)
- sendInvite (name on Discord or any other identifier)
- ban (@username) (reason (for doing ...))
- compelOwner (action to be performed by owner)
- changeVoteDuration (new time, in seconds, to change duration to)
- changeQuorum (new percentage of yea's needed to pass a measure, as a decimal (i.e. 0.50))

Response to command will be posted in #voting. Form is as follows:

`A vote has been called on the issue of (gerund-form description of command) (parameters...). This vote will last for (setting on vote duration), and requires (setting on population vote count validity percentage)% of the server to vote in favor of the measure in order for it to pass. Vote by reacting with either the checkmark or red-x mark on this message.`

For example, if init input is `!callvote createChannel memes`, result will be:

`A vote has been called on the issue of creating a channel named memes. This vote will last for 24 hours, and requires 51% of the server to vote in favor of the measure in order for it to pass. Vote by reacting with either the checkmark or red-x mark on this message.`

## Misc Info

The bot can be configured such that invites may be issued by vote. To make this worthwhile, the server owner should prohibit anyone from creating an invite for the sever.

The bot will only handle one vote at a time. Therefore I personally recommend a voting period of no longer than 36 hours, although it is your perogative (or of the server's - as a vote can be called to change the voting time alloted).

I personally see this bot being used in the following way: #voting is a read-only channel (with perms to add reactions to messages) and an accompanying #voting-discussion or similar channel which is read/write and allows people to debate. No moderators or administrators besides the owner. It's your server, though; do what you will.

### Feasibility of Removing a "Server Owner"

You can establish dummy account to create a server and then "forget" the credentials, although this would obsolete compelOwner and sendInvite commands (as both require some "trusted" human to step in) and bind the server to perform only actions that can be performed by AdminBot.

## Known Issues

- Bot cannot run on multiple servers at once, and will always prefer the last server joined.

-----

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.