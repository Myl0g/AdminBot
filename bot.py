import asyncio
import botSettings
import discord
import discordHelpers


async def receiveVoteRequest(command: str, guild: discord.Guild):
    if not isValidCommand(command):
        return "A vote cannot be called by bot on that measure, as the command is not programmed. You could use the command 'compelOwner' to perform the action for you, but such a measure is entirely contingent on the owner's willingness to execute it."

    gerund, params, passAction = interpretVoteCommand(command, guild)

    message = await discordHelpers.sendMessage(guild, "voting", "A vote has been called on the issue of {gerund} {params}. This vote will last for {duration} seconds and requires at least {quorum}% of the server to vote in favor of the measure in order for it to pass. Vote by reacting with either the checkmark or red-X mark on this message.".format(
        gerund=gerund, params=" ".join(params), duration=botSettings.getVoteDurationInSeconds(guild.id), quorum=str(botSettings.getVoteQuorum(guild.id) * 100)))

    await discordHelpers.addReaction(message, "✅")
    await discordHelpers.addReaction(message, "❌")

    asyncio.ensure_future(queueVote(message, passAction))
    return ""


def isValidCommand(command: str):
    availableCommands = ["compelOwner", "createTextChannel", "createVoiceChannel",
                         "changeVoteDuration", "changeVoteQuorum", "ban", "sendInvite"]
    return command.split(" ")[0] in availableCommands


def interpretVoteCommand(command: str, guild: discord.Guild):
    # Requires valid command! Use isValidCommand(command) if unsure.
    li = command.split(" ")
    if li[0] == "compelOwner":
        return "compelling the server owner to", ["'{}'".format(" ".join(li[1:]))], {"function": discordHelpers.sendMessage, "params": [guild, discordHelpers.getChannel(guild, "voting"), "The server owner is hereby compelled by democratic vote to perform the following action: '{}'. Failure to perform this action will not have legal consequence, but may diminish the faith of the people in the server owner's ability to operate democratically.".format(" ".join(li[1:]))], "requiresAwait": True}

    if li[0] == "createTextChannel":
        return "creating a text channel named", [li[1]], {"function": guild.create_text_channel, "params": [li[1]], "requiresAwait": True}

    if li[0] == "createVoiceChannel":
        return "creating a voice channel named", [li[1]], {"function": guild.create_voice_channel, "params": [li[1]], "requiresAwait": True}

    if li[0] == "changeVoteDuration":
        return "changing the voting time for all measures to", [str(int(li[1])), "seconds"], {"function": botSettings.setenv, "params": [guild.id, "VOTE_DURATION_SECONDS", li[1]], "requiresAwait": False}

    if li[0] == "changeVoteQuorum":
        return "changing the required percentage of yes votes from the server to", [str(float(li[1]) * 100), "%"], {"function": botSettings.setenv, "params": [guild.id, "VOTE_QUORUM", float(li[1])], "requiresAwait": False}

    if li[0] == "ban":
        return "banning the user", [li[1], " ".join(li[2:])], {"function": discordHelpers.ban, "params": [guild, li[1], " ".join(li[2:])], "requiresAwait": True}

    if li[0] == "sendInvite":
        return "asking the server owner to send an invite to the user", [li[1]], {"function": discordHelpers.sendNewInviteToOwner, "params": [guild], "requiresAwait": True}


async def queueVote(message: discord.Message, passAction):
    await asyncio.sleep(botSettings.getVoteDurationInSeconds(message.guild.id))

    voteResult = await didVotePass(message.guild, message.id)

    if voteResult:
        await discordHelpers.sendMessage(message.guild, "voting", "The measure has passed, and the action will now execute.")

        if passAction["requiresAwait"]:
            await passAction["function"](*passAction["params"])
        else:
            passAction["function"](*passAction["params"])

    else:
        await discordHelpers.sendMessage(message.guild, "voting", "The measure did not pass, and nothing will happen.")


async def didVotePass(guild, messageID):
    numberYea = (await discordHelpers.getNumberYea(guild, messageID)) - 1

    result = (numberYea / discordHelpers.getRegisteredVoters(guild))

    return result >= botSettings.getVoteQuorum(guild.id)
