import discord
import bot

client = discord.Client()


@client.event
async def on_ready():
    print('Ready and loaded. Logged in as {0.user}'.format(client))


@client.event
async def on_message(message: discord.Message):
    if message.author == client.user:
        return

    if message.content.startswith('!callvote'):
        command = message.content.split(" ")

        response = await bot.receiveVoteRequest(" ".join(command[1:]), message.guild)
        if response:
            await message.channel.send(content=response)

    elif message.content.startswith('!help'):
        await message.channel.send(content="""
Use !callvote [command], where [command] is one of the following:

- createTextChannel [name]
- createVoiceChannel [name]
- sendInvite [name on Discord or any other identifier]
- ban [@username] [reason (for doing ...)]
- compelOwner [action to be performed by owner]
- changeVoteDuration [new time, in seconds, to change duration to]
- changeQuorum [new percentage of yea's needed to pass a measure, as a decimal (i.e. 0.50)]

Response to command will be posted in #voting.
        """)


async def sendMessage(guild: discord.Guild, channel: str, messageText: str):
    channelObj = getChannel(guild, channel)
    return await channelObj.send(content=messageText)


async def addReaction(messageObj: discord.Message, emoji):
    await messageObj.add_reaction(emoji)


async def createGeneralInvite(guild: discord.Guild):
    channelObj = getChannel(guild, "general")
    return await channelObj.create_invite(max_uses=1)


async def sendNewInviteToOwner(guild: discord.Guild):
    await sendDM(guild.owner, "The server members have asked that you distribute this invite to the person they have selected: {}".format(str(await createGeneralInvite())))


async def sendDM(user: discord.User, messageText: str):
    channel = await user.create_dm()
    await channel.send(messageText)


async def getNumberYea(guild: discord.Guild, messageID):
    channel = getChannel(guild, "voting")
    message = await channel.fetch_message(messageID)

    for reaction in message.reactions:
        if str(reaction.emoji) == "✅":
            return reaction.count

    return 0


def getRegisteredVoters(guild: discord.Guild):
    return len(guild.members) - 1


async def ban(guild: discord.Guild, userMention, reason: str):
    userObj = getGuildUserFromMention(userMention)
    await guild.ban(userObj, reason=reason)


def getGuildUserFromMention(guild: discord.Guild, userMention: str):
    for user in guild.members:
        if userMention.replace("!", "") == user.mention:
            return user


def getChannel(guild: discord.Guild, name: str):
    for channel in guild.channels:
        if str(channel) == name:
            return channel

    return getChannel(guild, "general")
