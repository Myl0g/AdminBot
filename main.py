#! /usr/bin/env python3

import discordHelpers
import os

envs = ["VOTE_DURATION_SECONDS", "VOTE_QUORUM", "DISCORD_TOKEN"]

for env in envs:
    if not os.getenv(env):
        print("You must set a value for {} before running this program.".format(env))
        exit(1)

discordHelpers.client.run(os.getenv("DISCORD_TOKEN"))
