import os
import json

# Settings are stored in botSettings.json
# i.e. {"Guild ID as str": {"VOTE_QUORUM": 0.501, "VOTE_DURATION_SECONDS": 30}}

settings = None
try:
    settings = json.load(open("botSettings.json", "r"))
except OSError:
    with open("botSettings.json", "w") as f:
        f.write("")

if not settings:
    settings = {}

def getVoteQuorum(guildID):
    try:
        return float(settings[str(guildID)]["VOTE_QUORUM"])
    except KeyError:
        return 0.501


def getVoteDurationInSeconds(guildID):
    try:
        return int(settings[str(guildID)]["VOTE_DURATION_SECONDS"])
    except KeyError:
        return 60


def setenv(guildID, varName, newValue):
    try:
        settings[str(guildID)][varName] = newValue
    except KeyError:
        settings[str(guildID)] = {}
        settings[str(guildID)][varName] = newValue

    with open("botSettings.json", "w") as f:
        f.write(json.dumps(settings))
